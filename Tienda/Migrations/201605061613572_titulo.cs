namespace Tienda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class titulo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Peliculas", "titulo", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Peliculas", "titulo", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
