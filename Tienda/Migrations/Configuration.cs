namespace Tienda.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Tienda.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Tienda.Models.PeliculaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Tienda.Models.PeliculaDBContext context)
        {
            Random rnd = new Random();
            for (int i = 0; i < 100; i++) { 
                context.peliculas.AddOrUpdate(
                  p => p.titulo,
                  new Pelicula { Director = "Dire 4" ,
                  titulo = "Peli  " + i ,
                  Fecha = DateTime.Parse("2011-1-11"),
                  ruta = "/Content/imagenes/HP" + rnd.Next(1,5).ToString() + ".jpg",
                  Rate = rnd.Next(1, 5)
                  }
                );
            }            
        }
    }
}
