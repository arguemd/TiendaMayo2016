﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tienda.Models;
using PagedList;
using System.IO;

namespace Tienda.Controllers
{
    public class PeliculasController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        // GET: Peliculas
        private int paginacion = 10;
        
        public ActionResult Index(int pagina = 1)
        {
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult Lista(int pagina = 1)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Resultados", db.peliculas.ToList().ToPagedList(pagina, paginacion));            
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult ListaImagenes(int pagina = 1)
        {
            var paginacion = 18;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ResultadosImagenes", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }


        // GET: Peliculas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // GET: Peliculas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Peliculas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,titulo,Fecha,Director,categoria,ruta,precio,description")] Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);

                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }

                db.peliculas.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pelicula);
        }

        // GET: Peliculas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,titulo,Fecha,Director,categoria,ruta,precio,description")] Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);

                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }
                db.Entry(pelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }

        // GET: Peliculas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            db.peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RateProduct(int id, int rate)
        {

            int userId = 142;
            bool success = false;
            string error = "";
            try
            {
                success = CalificarPelicula(userId, id, rate);
            }
            catch (System.Exception ex)
            {
                error = ex.Message;
            }
            return Json(new { error = error, success = success, pid = id },
                JsonRequestBehavior.AllowGet);

        }

        private bool CalificarPelicula(int userId, int id, int rate)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return false;
            }
            pelicula.Rate = rate;
            db.Entry(pelicula).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
