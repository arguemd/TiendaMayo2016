﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tienda.Controllers
{
    public class HomeController : Controller
    {
        //[OutputCache(Duration=10, VaryByParam= "none")]
         [OutputCache(Duration = 10, VaryByParam = "id")]
        public ActionResult Index(int id = 0)
        {
            //throw new Exception("error capa 8");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}