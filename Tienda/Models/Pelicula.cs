﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tienda.Models

{
    public enum Categoria
    {
        Ficcion,
        Drama,
        Comedia,
        Horrir

    }

    public class Pelicula{
    
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage= "EL campo {0} es requerido")]
        [StringLength(80, MinimumLength = 5, ErrorMessage= "El {0} debe ser de al menos {2} caracteres.")]
        [Display(Name = "Nombre de la pelicula")]
        public string titulo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        public string Director { get; set; }
        public Categoria? categoria {get; set;}
        public string ruta { get; set; }
        public decimal precio { get; set; }
        [StringLength(500, MinimumLength = 5,
            ErrorMessage = "El {0} debe ser de al menos {2} Caracteres.")]
        public string description { get; set; }
        public int Rate { get; set; }
    }
}