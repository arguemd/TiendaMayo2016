﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Tienda.Models
{
    public class PeliculaDBContext : DbContext
    {
        public PeliculaDBContext() : base("PeliculasConnection") { 
        
        }
        public DbSet<Pelicula> peliculas
        {
            get;
            set;
        }
    }
}